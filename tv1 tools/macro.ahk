﻿#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.
DetectHiddenWindows, On

#Include C:\Users\Kabayan Group\Desktop\automation\tv1 tools\env.ahk

arg1 = %VIDEOSRC%
arg2 = %AUDIOSRC%
arg3 = %RECORDPATH%
arg4 = %OUTPUTEXTENSION%

videoDevice = %arg1%
audioDevice = %arg2%

outputPath = %arg3%

ext = %arg4%

If (InStr(ext, "."))
    ext = %ext%
else
    ext = .%ext%

StringLower, extensionOutput, ext

oldWindowTitle = "untitled"

Loop
{
    FormatTime, FileTime, %A_Now%, %A_YYYY%_%A_MM%_%A_DD%_HH_mm_ss
    extension = %extensionOutput%
    fileName = %FileTime%%extension%
    windowTitle = %FileTime%RC

    Run, record_template.bat %windowTitle% "%videoDevice%" "%audioDevice%" "%outputPath%" "%fileName%"

    Sleep, 300000
    ; Sleep, 10000
    Loop 
    {
        ControlSend,, ^c, %windowTitle%
        IfWinNotActive, %windowTitle%
            Break
    }

    oldWindowTitle = %windowTitle%
}

^+!s::ExitApp