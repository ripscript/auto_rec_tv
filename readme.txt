syntax in target shortcut:
"C:\Users\Kabayan Group\Desktop\automation\macro.ahk" videoDevice audioDevice outputPath extension

example:
"C:\Users\Kabayan Group\Desktop\automation\macro.ahk" "OBS Virtual Camera" "Microphone Array (Realtek High Definition Audio)" "C:\Users\Kabayan Group\Videos\globaltv" "mp4"
