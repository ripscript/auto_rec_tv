#SingleInstance, Force
SendMode Input
SetWorkingDir, %A_ScriptDir%

WinGet, windowList, List, ahk_exe cmd.exe
Loop %windowList%
{
    WinGetTitle, WindowTitle, % WinTitle := "ahk_id" windowList%A_Index%
    If WinExist(WindowTitle)
        WinClose
}