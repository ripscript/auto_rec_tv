TITLE = %1

set videoDevice=%2
set audioDevice=%3
set outputDir=%4
set fileName=%5

ECHO %videoDevice%

ffmpeg -f dshow -rtbufsize 100M -video_size 1280x720 -framerate 60 -vcodec mjpeg -i video=%videoDevice%:audio=%audioDevice% -vcodec h264_qsv -b:v 7000k -acodec aac %outputDir%/%fileName%

pause