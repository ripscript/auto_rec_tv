array := AltTabWindows()

AltTabWindows() {
    static WS_EX_TOPMOST := 0x8 ; sets the Always On Top flag
    static WS_EX_APPWINDOW := 0x40000 ; provides a taskbar button
    static WS_EX_TOOLWINDOW := 0x80 ; removes the window from the alt-tab list
    static GW_OWNER := 4

    AltTabList := {}
    windowList := ""
    DetectHiddenWindows, Off ; makes DllCall("IsWindowVisible") unnecessary
    WinGet, windowList, List ; gather a list of running programs
    Loop, %windowList%
    {
        ownerID := windowID := windowList%A_Index%
        Loop { ;If the window we found is opened by another application or "child", let's get the hWnd of the parent
            ownerID := Format("0x{:x}", DllCall("GetWindow", "UInt", ownerID, "UInt", GW_OWNER))
        } Until !Format("0x{:x}", DllCall("GetWindow", "UInt", ownerID, "UInt", GW_OWNER))
        ownerID := ownerID ? ownerID : windowID

        ; only windows that are not removed from the Alt+Tab list, AND have a taskbar button, will be appended to our list.
        If (Format("0x{:x}", DllCall("GetLastActivePopup", "UInt", ownerID)) = windowID)
        {
            WinGet, es, ExStyle, ahk_id %windowID%
            If (!((es & WS_EX_TOOLWINDOW) && !(es & WS_EX_APPWINDOW)) && !IsInvisibleWin10BackgroundAppWindow(windowID))
                AltTabList.Push(windowID)
        }
    }
    Gui, Add, ListView, r20 w700, #|ID|Title
    Loop, % AltTabList.length()
    {
        WinGetTitle, title, % "ahk_id " AltTabList[A_Index]
        LV_Add("", A_Index, AltTabList[A_Index], title)
    }
    LV_ModifyCol() ; Auto-size each column to fit its contents.
    Gui, Show
    return

    GuiClose:
    Exitapp
    return AltTabList
}

IsInvisibleWin10BackgroundAppWindow(hWindow) {
    result := 0
    VarSetCapacity(cloakedVal, A_PtrSize) ; DWMWA_CLOAKED := 14
    hr := DllCall("DwmApi\DwmGetWindowAttribute", "Ptr", hWindow, "UInt", 14, "Ptr", &cloakedVal, "UInt", A_PtrSize)
    if !hr ; returns S_OK (which is zero) on success. Otherwise, it returns an HRESULT error code
        result := NumGet(cloakedVal) ; omitting the "&" performs better
    return result ? true : false
}